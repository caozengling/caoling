/**
 *  获取url传参工具类
 *  @Authors: LiRui
 *  @date 2017-09-19
 *
 */

var QueryUtils = {
    GetQueryString: function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }
};
