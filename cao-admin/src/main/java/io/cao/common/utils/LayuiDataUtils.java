package io.cao.common.utils;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.fasterxml.jackson.databind.util.JSONPObject;
import net.sf.json.JSONArray;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Created by caozengling on 2018/6/20.
 * layui 的返回参数
 */

public class LayuiDataUtils {
    public  static PageUtils getPageUtils(@RequestParam Map<String, Object> params, Pagination page, List<PageData> deptList) {
        return new PageUtils(deptList,page.getTotal(),Integer.parseInt(params.get("limit").toString()),Integer.parseInt(params.get("page").toString()));
    }
    public static void setPage(PageData pd, Pages page) {
        page.setCurrent(Integer.parseInt(pd.get("page").toString()));
        page.setSize(Integer.parseInt(pd.get("limit").toString()));
        page.setPd(pd);
    }
    public static String getJsonData(Pagination page, List<PageData> varList) {
        JSONArray jsonObject = JSONArray.fromObject(varList);
        String jsonStr = jsonObject.toString();
        return "{\"code\":0,\"msg\":\"\",\"count\":" + page.getTotal() + ",\"data\":"+jsonStr+"}";
    }
    /**
     * @param pd
     * @param map
     * @return
     */
    public static Object returnObject(PageData pd, Map map){
        if(pd.containsKey("callback")){
            String callback = pd.get("callback").toString();
            return new JSONPObject(callback, map);
        }else{
            return map;
        }
    }

}

