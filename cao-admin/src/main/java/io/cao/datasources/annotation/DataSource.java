package io.cao.datasources.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解
 * @author caozengling
 * @email caozengling@qq.com
 * @date 2018-06-25
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}
