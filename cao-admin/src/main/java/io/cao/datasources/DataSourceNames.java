package io.cao.datasources;

/**
 * 增加多数据源，在此配置
 *
 * @author caozengling
 * @email caozengling@qq.com
 * @date 2018-06-25
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";

}
