/**
 * Copyright 2018 czl
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.cao.modules.oss.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.cao.modules.oss.entity.SysOssEntity;

/**
 * 文件上传
 * 
 * @author caozengling
 * @email caozengling@qq.com
 * @date 2018-06-25
 */
public interface SysOssDao extends BaseMapper<SysOssEntity> {
	
}
