package io.cao.modules.demo.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.cao.common.utils.PageData;
import io.cao.common.utils.Pages;
import io.cao.modules.demo.entity.DemoEntity;

import java.util.List;

/**
 * 系统用户
 * 
 * @author caozegnling
 * @email 32595667@qq.com
 * @date 2018-06-13 23:29:03
 */
public interface DemoDao extends BaseMapper<DemoEntity> {
    /**
     * 查询用户的所有菜单ID
     */
    List<PageData> selectUserList(Pages page, PageData pd);

    //更新
    void updateList(PageData pd);

}
