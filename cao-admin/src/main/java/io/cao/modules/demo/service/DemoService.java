package io.cao.modules.demo.service;

import com.baomidou.mybatisplus.service.IService;
import io.cao.common.utils.Pages;
import io.cao.common.utils.PageData;
import io.cao.common.utils.PageUtils;
import io.cao.modules.demo.entity.DemoEntity;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author caozegnling
 * @email 32595667@qq.com
 * @date 2018-06-13 23:29:03
 */
public interface DemoService extends IService<DemoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    /**
     * 获取子部门ID，用于数据过滤
     */
    List<PageData>  selectUserList(Pages page,PageData pd);

    void  updateList(PageData pd);
}

