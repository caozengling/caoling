package io.cao.modules.demo.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.cao.common.annotation.DataFilter;
import io.cao.common.utils.PageData;
import io.cao.common.utils.PageUtils;
import io.cao.common.utils.Pages;
import io.cao.common.utils.Query;
import io.cao.modules.demo.dao.DemoDao;
import io.cao.modules.demo.entity.DemoEntity;
import io.cao.modules.demo.service.DemoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("demoService")
public class DemoServiceImpl extends ServiceImpl<DemoDao, DemoEntity> implements DemoService {


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DemoEntity> page = this.selectPage(
                new Query<DemoEntity>(params).getPage(),
                new EntityWrapper<DemoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<PageData> selectUserList(Pages page, PageData pd) {
        System.out.println(page);
        return baseMapper.selectUserList(page,pd);
    }

    @Override
    public void updateList(PageData pd) {
        baseMapper.updateList(pd);
    }

}
