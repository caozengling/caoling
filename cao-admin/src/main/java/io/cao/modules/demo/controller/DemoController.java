package io.cao.modules.demo.controller;

import io.cao.common.utils.Pages;
import io.cao.common.utils.PageData;
import io.cao.common.utils.R;
import io.cao.common.utils.RedisUtils;
import io.cao.modules.demo.entity.DemoEntity;
import io.cao.modules.demo.service.DemoService;
import io.cao.modules.sys.base.BaseController;
import io.cao.modules.sys.entity.SysUserEntity;
import io.cao.modules.sys.shiro.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.cao.common.utils.LayuiDataUtils.setPage;


/**
 * 系统用户
 *
 * @author caozengling
 * @email 32595667@qq.com
 * @date 2018-06-13 23:29:03
 */
@Transactional
@RestController
@RequestMapping("DemoController")
public class DemoController extends BaseController {
    @Autowired
    private DemoService demoService;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("chakan:list")
    public R list(Pages page) {
//        Pagination pp= new Pagination();//重写分页
        PageData pd=this.getPageData();
//        PageUtils page = demoService.queryPage(params);
        setPage(pd, page);//设置分页参数
        List<PageData> dataList= new ArrayList<>();
        /*if(redisUtils.get("dataList")!=null){
            System.out.println(redisUtils.get("dataList"));
        }else {
            dataList=demoService.selectUserList(page, params);
            redisUtils.set("dataList",dataList);//保存redis 缓存数据
        }*/
        SysUserEntity user = ShiroUtils.getUserEntity();
        dataList=demoService.selectUserList(page,pd);
        return R.ok().put("data", dataList).put("count",page.getTotal());
    }




    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
    @RequiresPermissions("demo:demo:info")
    public R info(@PathVariable("userId") Long userId){
        Object demo = demoService.selectById(userId);

        return R.ok().put("data", demo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("save")
    public R save(@RequestBody DemoEntity demo){
        demoService.insert(demo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("demo:demo:update")
    @ResponseBody
    public R update(){
        PageData pd=this.getPageData();
        System.out.println(pd);
        demoService.updateList(pd);
//        ValidatorUtils.validateEntity(demo);
//        demoService.updateAllColumnById(demo);//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("demo:demo:delete")
    public R delete(@RequestBody Long[] userIds){
        demoService.deleteBatchIds(Arrays.asList(userIds));

        return R.ok();
    }

}
