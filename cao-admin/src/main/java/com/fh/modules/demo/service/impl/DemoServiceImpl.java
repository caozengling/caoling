package com.fh.modules.demo.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fh.modules.demo.dao.DemoDao1;
import com.fh.modules.demo.entity.DemoEntity;
import com.fh.modules.demo.service.DemoService;
import io.cao.common.utils.PageUtils;
import io.cao.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("demoService")
public class DemoServiceImpl extends ServiceImpl<DemoDao1, DemoEntity> implements DemoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DemoEntity> page = this.selectPage(
                new Query<DemoEntity>(params).getPage(),
                new EntityWrapper<DemoEntity>()
        );

        return new PageUtils(page);
    }

}
