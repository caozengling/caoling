package com.fh.modules.demo.dao;

import com.fh.modules.demo.entity.DemoEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 系统用户
 * 
 * @author caozegnling
 * @email 32595667@qq.com
 * @date 2018-06-13 23:35:14
 */
public interface DemoDao1 extends BaseMapper<DemoEntity> {
	
}
