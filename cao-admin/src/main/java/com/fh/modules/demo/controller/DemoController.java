package com.fh.modules.demo.controller;

import com.fh.modules.demo.entity.DemoEntity;
import com.fh.modules.demo.service.DemoService;
import io.cao.common.utils.PageUtils;
import io.cao.common.utils.R;
import io.cao.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 系统用户
 *
 * @author caozegnling
 * @email 32595667@qq.com
 * @date 2018-06-13 23:35:14
 */
@RestController
@RequestMapping("demo111/demo111")
public class DemoController {
    @Autowired
    private DemoService demoService;

    /**
     * 列表
     */
    @RequestMapping("/list1111111111111")
    @RequiresPermissions("demo:demo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = demoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
    @RequiresPermissions("demo:demo:info")
    public R info(@PathVariable("userId") Long userId){
        DemoEntity demo = demoService.selectById(userId);

        return R.ok().put("data", demo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("demo:demo:save")
    public R save(@RequestBody DemoEntity demo){
        demoService.insert(demo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("demo:demo:update")
    public R update(@RequestBody DemoEntity demo){
        ValidatorUtils.validateEntity(demo);
        demoService.updateAllColumnById(demo);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("demo:demo:delete")
    public R delete(@RequestBody Long[] userIds){
        demoService.deleteBatchIds(Arrays.asList(userIds));

        return R.ok();
    }

}
