package com.fh.modules.demo.service;

import com.baomidou.mybatisplus.service.IService;
import com.fh.modules.demo.entity.DemoEntity;
import io.cao.common.utils.PageUtils;

import java.util.Map;

/**
 * 系统用户
 *
 * @author caozegnling
 * @email 32595667@qq.com
 * @date 2018-06-13 23:35:14
 */
public interface DemoService extends IService<DemoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

